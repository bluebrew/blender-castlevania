# Castlevania Scene

* Engine: Eevee
* Version: 2.83 LTS


### _scene.blend

This is the main blend file that should be used for rendering. The various assets used in the scene are linked to this scene.

This blend file contains the various lights and probes, and the camera.

### /props/

This folder contains the various props used in the scene. Adjustments to the models or materials used should be made in the reevant assets file, as they are linked to the main scene so updates will be used automatically.